drop database PRN292
drop table UserLogins
drop table Users
drop table Songs
drop table Albums
drop table Geners
create database PRN292
use PRN292
go
create table Geners (
	GenersID int not null identity(1,1) primary key,
	GenersName nvarchar(255) not null,
)
go
create table Albums(
	AlbumID int not null identity(1,1) primary key,
	AlbumName nvarchar(255) not null,
)

go
 create table Songs(
	SongID   int not null identity(1,1) primary key,
	GenersID int not null  foreign key references Geners(GenersID),
	MusicianName nvarchar(200) not null,
	SingerName nvarchar(200) not null,
	CreateDate date not null,
	Rate nvarchar(10),
	Lyric text,
	pathFile nvarchar(255) not null,

)

go
create table Users(
UserID nvarchar(20) not null,
Name nvarchar(255) not null,
UserAge int ,
UserEmail nvarchar(100) not null,
UserPhone varchar(20) ,
UserAdress nvarchar(150),
Status bit not null,
Note nvarchar(255),
CreateDate date not null,
primary key(UserID)
)
go
create table UserLogins(
UserID nvarchar(20) not null primary key foreign key references Users(UserID),
LoginPwd nvarchar(30) not null,
)
go
create table UserAlbums(
	AlbumID  int not null foreign key references Albums(AlbumID),
	UserID  nvarchar(20)  not null   foreign key references Users(UserID),
)
go 
create table UserSong(
	UserID  nvarchar(20)  not null   foreign key references Users(UserID),
	SongID   int not null foreign key references Songs(SongID)
)
drop table UserSong
create table UserSong(
	UserID  nvarchar(20)  not null   foreign key references Users(UserID),
	SongID int not null  primary key  foreign key references Songs(SongID)
)
go 
create table SongAlbum(
		AlbumID  int  foreign key references Albums(AlbumID),
		SongID   int not null foreign key references Songs(SongID)
)
insert into Geners values('V-POP')
SELECT * FROM Geners
insert into Songs values(1,'tan','tan','2017/2/23','***','','E:/')
select * from Songs


insert into Users values('tan123','Tan',20,'tan@123.123','123123123','HN',1,'','2017/2/23')
insert into Users values('Manh123','Manh',19,'manh@123.1232','123123123','HN',0,'','2017/2/23')
select * from Users
insert into UserLogins values('tan123','123')
insert into UserLogins values('manh123','1233')
select * from UserLogins

insert into UserSong values('tan123',1)
insert into UserSong values('tan1234',1)
SELECT * FROM UserSong

SELECt * FROM Albums
INSERT INTO Albums values('New ALbum 2')

SELECT * FROM UserAlbums
insert into UserAlbums values(1,'tan1234')
insert into UserAlbums values(2,'tan1234')

SELECT * FROM SongAlbum
INSERT INTO SongAlbum values(1,1)
