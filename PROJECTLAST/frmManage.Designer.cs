﻿namespace PROJECTLAST
{
    partial class frmManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManage));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvListUserU = new System.Windows.Forms.DataGridView();
            this.gbConfirmU = new System.Windows.Forms.GroupBox();
            this.dgvConfirmU = new System.Windows.Forms.DataGridView();
            this.gbFuntionU = new System.Windows.Forms.GroupBox();
            this.btnLogOutU = new System.Windows.Forms.Button();
            this.btnSaveU = new System.Windows.Forms.Button();
            this.btnDelU = new System.Windows.Forms.Button();
            this.btnUpdateU = new System.Windows.Forms.Button();
            this.btnAddU = new System.Windows.Forms.Button();
            this.gbSearchU = new System.Windows.Forms.GroupBox();
            this.lbToSearchU = new System.Windows.Forms.Label();
            this.lbFromSearchU = new System.Windows.Forms.Label();
            this.dtToSearchU = new System.Windows.Forms.DateTimePicker();
            this.dtFromSearchU = new System.Windows.Forms.DateTimePicker();
            this.tbSearchU = new System.Windows.Forms.TextBox();
            this.rbDateSearchU = new System.Windows.Forms.RadioButton();
            this.lbDateSearchU = new System.Windows.Forms.Label();
            this.rbNameSearchU = new System.Windows.Forms.RadioButton();
            this.rbUserNameSearchU = new System.Windows.Forms.RadioButton();
            this.lbNameSearchU = new System.Windows.Forms.Label();
            this.lbUserNameSearchU = new System.Windows.Forms.Label();
            this.lbMainTitleU = new System.Windows.Forms.Label();
            this.gbDetailU = new System.Windows.Forms.GroupBox();
            this.tbPwdU = new System.Windows.Forms.TextBox();
            this.lbPassU = new System.Windows.Forms.Label();
            this.tbUserNameU = new System.Windows.Forms.TextBox();
            this.lbUserNameU = new System.Windows.Forms.Label();
            this.dtDateCreateU = new System.Windows.Forms.DateTimePicker();
            this.lbDateCreateU = new System.Windows.Forms.Label();
            this.tbNoteU = new System.Windows.Forms.TextBox();
            this.lbNoteU = new System.Windows.Forms.Label();
            this.tbAgeU = new System.Windows.Forms.TextBox();
            this.lbAgeU = new System.Windows.Forms.Label();
            this.tbAdrrU = new System.Windows.Forms.TextBox();
            this.lbAdrrU = new System.Windows.Forms.Label();
            this.tbEmailU = new System.Windows.Forms.TextBox();
            this.lbEmailU = new System.Windows.Forms.Label();
            this.tbPhoneU = new System.Windows.Forms.TextBox();
            this.lbPhoneU = new System.Windows.Forms.Label();
            this.tbNameU = new System.Windows.Forms.TextBox();
            this.lbNameU = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewM = new System.Windows.Forms.DataGridView();
            this.gbFuntionM = new System.Windows.Forms.GroupBox();
            this.btnLogOutM = new System.Windows.Forms.Button();
            this.btListenM = new System.Windows.Forms.Button();
            this.btSaveM = new System.Windows.Forms.Button();
            this.btDelM = new System.Windows.Forms.Button();
            this.btUpdateM = new System.Windows.Forms.Button();
            this.btAddM = new System.Windows.Forms.Button();
            this.gbSearchM = new System.Windows.Forms.GroupBox();
            this.lbToSearchM = new System.Windows.Forms.Label();
            this.lbFromSearchM = new System.Windows.Forms.Label();
            this.dtToSearchM = new System.Windows.Forms.DateTimePicker();
            this.dtFromSearchM = new System.Windows.Forms.DateTimePicker();
            this.tbSearchM = new System.Windows.Forms.TextBox();
            this.rbDateSearchM = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.rbNameSearchM = new System.Windows.Forms.RadioButton();
            this.rbIDM = new System.Windows.Forms.RadioButton();
            this.lbNameSearchM = new System.Windows.Forms.Label();
            this.lbIDSearchM = new System.Windows.Forms.Label();
            this.lbMainTitleM = new System.Windows.Forms.Label();
            this.gbDetailM = new System.Windows.Forms.GroupBox();
            this.numRateM = new System.Windows.Forms.NumericUpDown();
            this.dtDateCreateM = new System.Windows.Forms.DateTimePicker();
            this.lbDateM = new System.Windows.Forms.Label();
            this.tbTypeM = new System.Windows.Forms.TextBox();
            this.lbTypeM = new System.Windows.Forms.Label();
            this.tbLyricM = new System.Windows.Forms.TextBox();
            this.lbLyricM = new System.Windows.Forms.Label();
            this.lbRateM = new System.Windows.Forms.Label();
            this.tbAlbumM = new System.Windows.Forms.TextBox();
            this.lbAlbumM = new System.Windows.Forms.Label();
            this.tbSingerM = new System.Windows.Forms.TextBox();
            this.lbSingerM = new System.Windows.Forms.Label();
            this.tbAuthorM = new System.Windows.Forms.TextBox();
            this.lbAuthorM = new System.Windows.Forms.Label();
            this.tbNameM = new System.Windows.Forms.TextBox();
            this.lbNameM = new System.Windows.Forms.Label();
            this.tbIDM = new System.Windows.Forms.TextBox();
            this.lbIDM = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListUserU)).BeginInit();
            this.gbConfirmU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfirmU)).BeginInit();
            this.gbFuntionU.SuspendLayout();
            this.gbSearchU.SuspendLayout();
            this.gbDetailU.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewM)).BeginInit();
            this.gbFuntionM.SuspendLayout();
            this.gbSearchM.SuspendLayout();
            this.gbDetailM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRateM)).BeginInit();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvListUserU);
            this.tabPage2.Controls.Add(this.gbConfirmU);
            this.tabPage2.Controls.Add(this.gbFuntionU);
            this.tabPage2.Controls.Add(this.gbSearchU);
            this.tabPage2.Controls.Add(this.lbMainTitleU);
            this.tabPage2.Controls.Add(this.gbDetailU);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(909, 672);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Quản lí người dùng";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvListUserU
            // 
            this.dgvListUserU.AllowUserToAddRows = false;
            this.dgvListUserU.AllowUserToDeleteRows = false;
            this.dgvListUserU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListUserU.Location = new System.Drawing.Point(8, 508);
            this.dgvListUserU.Name = "dgvListUserU";
            this.dgvListUserU.ReadOnly = true;
            this.dgvListUserU.Size = new System.Drawing.Size(886, 158);
            this.dgvListUserU.TabIndex = 5;
            this.dgvListUserU.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListUserU_CellContentClick);
            // 
            // gbConfirmU
            // 
            this.gbConfirmU.Controls.Add(this.dgvConfirmU);
            this.gbConfirmU.Location = new System.Drawing.Point(8, 238);
            this.gbConfirmU.Name = "gbConfirmU";
            this.gbConfirmU.Size = new System.Drawing.Size(735, 145);
            this.gbConfirmU.TabIndex = 2;
            this.gbConfirmU.TabStop = false;
            this.gbConfirmU.Text = "Danh sách đăng kí chưa được thông qua";
            // 
            // dgvConfirmU
            // 
            this.dgvConfirmU.AllowUserToAddRows = false;
            this.dgvConfirmU.AllowUserToDeleteRows = false;
            this.dgvConfirmU.AllowUserToResizeRows = false;
            this.dgvConfirmU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConfirmU.Location = new System.Drawing.Point(7, 20);
            this.dgvConfirmU.Name = "dgvConfirmU";
            this.dgvConfirmU.ReadOnly = true;
            this.dgvConfirmU.ShowEditingIcon = false;
            this.dgvConfirmU.Size = new System.Drawing.Size(745, 119);
            this.dgvConfirmU.TabIndex = 0;
            this.dgvConfirmU.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConfirmU_CellContentClick);
            // 
            // gbFuntionU
            // 
            this.gbFuntionU.Controls.Add(this.btnLogOutU);
            this.gbFuntionU.Controls.Add(this.btnSaveU);
            this.gbFuntionU.Controls.Add(this.btnDelU);
            this.gbFuntionU.Controls.Add(this.btnUpdateU);
            this.gbFuntionU.Controls.Add(this.btnAddU);
            this.gbFuntionU.Location = new System.Drawing.Point(759, 40);
            this.gbFuntionU.Name = "gbFuntionU";
            this.gbFuntionU.Size = new System.Drawing.Size(135, 343);
            this.gbFuntionU.TabIndex = 3;
            this.gbFuntionU.TabStop = false;
            this.gbFuntionU.Text = "Tính năng";
            // 
            // btnLogOutU
            // 
            this.btnLogOutU.Location = new System.Drawing.Point(30, 182);
            this.btnLogOutU.Name = "btnLogOutU";
            this.btnLogOutU.Size = new System.Drawing.Size(75, 23);
            this.btnLogOutU.TabIndex = 4;
            this.btnLogOutU.Text = "Đăng xuất";
            this.btnLogOutU.UseVisualStyleBackColor = true;
            // 
            // btnSaveU
            // 
            this.btnSaveU.Enabled = false;
            this.btnSaveU.Location = new System.Drawing.Point(30, 143);
            this.btnSaveU.Name = "btnSaveU";
            this.btnSaveU.Size = new System.Drawing.Size(75, 23);
            this.btnSaveU.TabIndex = 3;
            this.btnSaveU.Text = "Lưu";
            this.btnSaveU.UseVisualStyleBackColor = true;
            this.btnSaveU.Click += new System.EventHandler(this.btnSaveU_Click);
            // 
            // btnDelU
            // 
            this.btnDelU.Location = new System.Drawing.Point(30, 106);
            this.btnDelU.Name = "btnDelU";
            this.btnDelU.Size = new System.Drawing.Size(75, 23);
            this.btnDelU.TabIndex = 2;
            this.btnDelU.Text = "Xóa";
            this.btnDelU.UseVisualStyleBackColor = true;
            // 
            // btnUpdateU
            // 
            this.btnUpdateU.Location = new System.Drawing.Point(30, 70);
            this.btnUpdateU.Name = "btnUpdateU";
            this.btnUpdateU.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateU.TabIndex = 1;
            this.btnUpdateU.Text = "Sửa";
            this.btnUpdateU.UseVisualStyleBackColor = true;
            this.btnUpdateU.Click += new System.EventHandler(this.btnUpdateU_Click);
            // 
            // btnAddU
            // 
            this.btnAddU.Location = new System.Drawing.Point(30, 32);
            this.btnAddU.Name = "btnAddU";
            this.btnAddU.Size = new System.Drawing.Size(75, 23);
            this.btnAddU.TabIndex = 0;
            this.btnAddU.Text = "Thêm";
            this.btnAddU.UseVisualStyleBackColor = true;
            // 
            // gbSearchU
            // 
            this.gbSearchU.Controls.Add(this.lbToSearchU);
            this.gbSearchU.Controls.Add(this.lbFromSearchU);
            this.gbSearchU.Controls.Add(this.dtToSearchU);
            this.gbSearchU.Controls.Add(this.dtFromSearchU);
            this.gbSearchU.Controls.Add(this.tbSearchU);
            this.gbSearchU.Controls.Add(this.rbDateSearchU);
            this.gbSearchU.Controls.Add(this.lbDateSearchU);
            this.gbSearchU.Controls.Add(this.rbNameSearchU);
            this.gbSearchU.Controls.Add(this.rbUserNameSearchU);
            this.gbSearchU.Controls.Add(this.lbNameSearchU);
            this.gbSearchU.Controls.Add(this.lbUserNameSearchU);
            this.gbSearchU.Location = new System.Drawing.Point(8, 391);
            this.gbSearchU.Name = "gbSearchU";
            this.gbSearchU.Size = new System.Drawing.Size(886, 100);
            this.gbSearchU.TabIndex = 4;
            this.gbSearchU.TabStop = false;
            this.gbSearchU.Text = "Tìm kiếm";
            // 
            // lbToSearchU
            // 
            this.lbToSearchU.AutoSize = true;
            this.lbToSearchU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbToSearchU.Location = new System.Drawing.Point(503, 55);
            this.lbToSearchU.Name = "lbToSearchU";
            this.lbToSearchU.Size = new System.Drawing.Size(30, 15);
            this.lbToSearchU.TabIndex = 9;
            this.lbToSearchU.Text = "Đến";
            // 
            // lbFromSearchU
            // 
            this.lbFromSearchU.AutoSize = true;
            this.lbFromSearchU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFromSearchU.Location = new System.Drawing.Point(324, 55);
            this.lbFromSearchU.Name = "lbFromSearchU";
            this.lbFromSearchU.Size = new System.Drawing.Size(21, 15);
            this.lbFromSearchU.TabIndex = 7;
            this.lbFromSearchU.Text = "Từ";
            // 
            // dtToSearchU
            // 
            this.dtToSearchU.Location = new System.Drawing.Point(539, 55);
            this.dtToSearchU.Name = "dtToSearchU";
            this.dtToSearchU.Size = new System.Drawing.Size(145, 20);
            this.dtToSearchU.TabIndex = 10;
            // 
            // dtFromSearchU
            // 
            this.dtFromSearchU.Location = new System.Drawing.Point(351, 55);
            this.dtFromSearchU.Name = "dtFromSearchU";
            this.dtFromSearchU.Size = new System.Drawing.Size(145, 20);
            this.dtFromSearchU.TabIndex = 8;
            // 
            // tbSearchU
            // 
            this.tbSearchU.Location = new System.Drawing.Point(18, 55);
            this.tbSearchU.Name = "tbSearchU";
            this.tbSearchU.Size = new System.Drawing.Size(248, 20);
            this.tbSearchU.TabIndex = 4;
            // 
            // rbDateSearchU
            // 
            this.rbDateSearchU.AutoSize = true;
            this.rbDateSearchU.Location = new System.Drawing.Point(329, 27);
            this.rbDateSearchU.Name = "rbDateSearchU";
            this.rbDateSearchU.Size = new System.Drawing.Size(14, 13);
            this.rbDateSearchU.TabIndex = 5;
            this.rbDateSearchU.TabStop = true;
            this.rbDateSearchU.UseVisualStyleBackColor = true;
            // 
            // lbDateSearchU
            // 
            this.lbDateSearchU.AutoSize = true;
            this.lbDateSearchU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDateSearchU.Location = new System.Drawing.Point(349, 25);
            this.lbDateSearchU.Name = "lbDateSearchU";
            this.lbDateSearchU.Size = new System.Drawing.Size(131, 15);
            this.lbDateSearchU.TabIndex = 6;
            this.lbDateSearchU.Text = "tìm kiếm theo thời gian";
            // 
            // rbNameSearchU
            // 
            this.rbNameSearchU.AutoSize = true;
            this.rbNameSearchU.Location = new System.Drawing.Point(170, 27);
            this.rbNameSearchU.Name = "rbNameSearchU";
            this.rbNameSearchU.Size = new System.Drawing.Size(14, 13);
            this.rbNameSearchU.TabIndex = 2;
            this.rbNameSearchU.TabStop = true;
            this.rbNameSearchU.UseVisualStyleBackColor = true;
            // 
            // rbUserNameSearchU
            // 
            this.rbUserNameSearchU.AutoSize = true;
            this.rbUserNameSearchU.Location = new System.Drawing.Point(18, 23);
            this.rbUserNameSearchU.Name = "rbUserNameSearchU";
            this.rbUserNameSearchU.Size = new System.Drawing.Size(14, 13);
            this.rbUserNameSearchU.TabIndex = 0;
            this.rbUserNameSearchU.TabStop = true;
            this.rbUserNameSearchU.UseVisualStyleBackColor = true;
            // 
            // lbNameSearchU
            // 
            this.lbNameSearchU.AutoSize = true;
            this.lbNameSearchU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNameSearchU.Location = new System.Drawing.Point(190, 25);
            this.lbNameSearchU.Name = "lbNameSearchU";
            this.lbNameSearchU.Size = new System.Drawing.Size(28, 15);
            this.lbNameSearchU.TabIndex = 3;
            this.lbNameSearchU.Text = "Tên";
            // 
            // lbUserNameSearchU
            // 
            this.lbUserNameSearchU.AutoSize = true;
            this.lbUserNameSearchU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUserNameSearchU.Location = new System.Drawing.Point(38, 21);
            this.lbUserNameSearchU.Name = "lbUserNameSearchU";
            this.lbUserNameSearchU.Size = new System.Drawing.Size(81, 15);
            this.lbUserNameSearchU.TabIndex = 1;
            this.lbUserNameSearchU.Text = "Tên tài khoản";
            // 
            // lbMainTitleU
            // 
            this.lbMainTitleU.AutoSize = true;
            this.lbMainTitleU.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitleU.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbMainTitleU.Location = new System.Drawing.Point(175, 18);
            this.lbMainTitleU.Name = "lbMainTitleU";
            this.lbMainTitleU.Size = new System.Drawing.Size(361, 18);
            this.lbMainTitleU.TabIndex = 0;
            this.lbMainTitleU.Text = "Quản lí danh sách người dùng trong kho dữ liệu";
            // 
            // gbDetailU
            // 
            this.gbDetailU.Controls.Add(this.tbPwdU);
            this.gbDetailU.Controls.Add(this.lbPassU);
            this.gbDetailU.Controls.Add(this.tbUserNameU);
            this.gbDetailU.Controls.Add(this.lbUserNameU);
            this.gbDetailU.Controls.Add(this.dtDateCreateU);
            this.gbDetailU.Controls.Add(this.lbDateCreateU);
            this.gbDetailU.Controls.Add(this.tbNoteU);
            this.gbDetailU.Controls.Add(this.lbNoteU);
            this.gbDetailU.Controls.Add(this.tbAgeU);
            this.gbDetailU.Controls.Add(this.lbAgeU);
            this.gbDetailU.Controls.Add(this.tbAdrrU);
            this.gbDetailU.Controls.Add(this.lbAdrrU);
            this.gbDetailU.Controls.Add(this.tbEmailU);
            this.gbDetailU.Controls.Add(this.lbEmailU);
            this.gbDetailU.Controls.Add(this.tbPhoneU);
            this.gbDetailU.Controls.Add(this.lbPhoneU);
            this.gbDetailU.Controls.Add(this.tbNameU);
            this.gbDetailU.Controls.Add(this.lbNameU);
            this.gbDetailU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDetailU.ForeColor = System.Drawing.SystemColors.Desktop;
            this.gbDetailU.Location = new System.Drawing.Point(8, 39);
            this.gbDetailU.Name = "gbDetailU";
            this.gbDetailU.Size = new System.Drawing.Size(735, 183);
            this.gbDetailU.TabIndex = 1;
            this.gbDetailU.TabStop = false;
            this.gbDetailU.Text = "Thông tin chi tiết";
            // 
            // tbPwdU
            // 
            this.tbPwdU.Enabled = false;
            this.tbPwdU.Location = new System.Drawing.Point(102, 68);
            this.tbPwdU.Name = "tbPwdU";
            this.tbPwdU.Size = new System.Drawing.Size(141, 22);
            this.tbPwdU.TabIndex = 5;
            // 
            // lbPassU
            // 
            this.lbPassU.AutoSize = true;
            this.lbPassU.Location = new System.Drawing.Point(7, 71);
            this.lbPassU.Name = "lbPassU";
            this.lbPassU.Size = new System.Drawing.Size(62, 16);
            this.lbPassU.TabIndex = 4;
            this.lbPassU.Text = "Mật khẩu";
            // 
            // tbUserNameU
            // 
            this.tbUserNameU.Enabled = false;
            this.tbUserNameU.Location = new System.Drawing.Point(102, 24);
            this.tbUserNameU.Name = "tbUserNameU";
            this.tbUserNameU.Size = new System.Drawing.Size(141, 22);
            this.tbUserNameU.TabIndex = 3;
            // 
            // lbUserNameU
            // 
            this.lbUserNameU.AutoSize = true;
            this.lbUserNameU.Location = new System.Drawing.Point(7, 27);
            this.lbUserNameU.Name = "lbUserNameU";
            this.lbUserNameU.Size = new System.Drawing.Size(89, 16);
            this.lbUserNameU.TabIndex = 2;
            this.lbUserNameU.Text = "Tên tài khoản";
            // 
            // dtDateCreateU
            // 
            this.dtDateCreateU.Enabled = false;
            this.dtDateCreateU.Location = new System.Drawing.Point(581, 63);
            this.dtDateCreateU.Name = "dtDateCreateU";
            this.dtDateCreateU.Size = new System.Drawing.Size(141, 22);
            this.dtDateCreateU.TabIndex = 11;
            // 
            // lbDateCreateU
            // 
            this.lbDateCreateU.AutoSize = true;
            this.lbDateCreateU.Location = new System.Drawing.Point(518, 68);
            this.lbDateCreateU.Name = "lbDateCreateU";
            this.lbDateCreateU.Size = new System.Drawing.Size(63, 16);
            this.lbDateCreateU.TabIndex = 6;
            this.lbDateCreateU.Text = "Ngày tạo";
            // 
            // tbNoteU
            // 
            this.tbNoteU.Enabled = false;
            this.tbNoteU.Location = new System.Drawing.Point(102, 149);
            this.tbNoteU.Name = "tbNoteU";
            this.tbNoteU.Size = new System.Drawing.Size(620, 22);
            this.tbNoteU.TabIndex = 13;
            // 
            // lbNoteU
            // 
            this.lbNoteU.AutoSize = true;
            this.lbNoteU.Location = new System.Drawing.Point(7, 152);
            this.lbNoteU.Name = "lbNoteU";
            this.lbNoteU.Size = new System.Drawing.Size(52, 16);
            this.lbNoteU.TabIndex = 12;
            this.lbNoteU.Text = "Ghi chú";
            // 
            // tbAgeU
            // 
            this.tbAgeU.Enabled = false;
            this.tbAgeU.Location = new System.Drawing.Point(358, 27);
            this.tbAgeU.Name = "tbAgeU";
            this.tbAgeU.Size = new System.Drawing.Size(141, 22);
            this.tbAgeU.TabIndex = 7;
            // 
            // lbAgeU
            // 
            this.lbAgeU.AutoSize = true;
            this.lbAgeU.Location = new System.Drawing.Point(266, 27);
            this.lbAgeU.Name = "lbAgeU";
            this.lbAgeU.Size = new System.Drawing.Size(35, 16);
            this.lbAgeU.TabIndex = 10;
            this.lbAgeU.Text = "Tuổi";
            // 
            // tbAdrrU
            // 
            this.tbAdrrU.Enabled = false;
            this.tbAdrrU.Location = new System.Drawing.Point(581, 24);
            this.tbAdrrU.Name = "tbAdrrU";
            this.tbAdrrU.Size = new System.Drawing.Size(141, 22);
            this.tbAdrrU.TabIndex = 10;
            // 
            // lbAdrrU
            // 
            this.lbAdrrU.AutoSize = true;
            this.lbAdrrU.Location = new System.Drawing.Point(518, 30);
            this.lbAdrrU.Name = "lbAdrrU";
            this.lbAdrrU.Size = new System.Drawing.Size(48, 16);
            this.lbAdrrU.TabIndex = 18;
            this.lbAdrrU.Text = "Địa chỉ";
            // 
            // tbEmailU
            // 
            this.tbEmailU.Enabled = false;
            this.tbEmailU.Location = new System.Drawing.Point(358, 65);
            this.tbEmailU.Name = "tbEmailU";
            this.tbEmailU.Size = new System.Drawing.Size(141, 22);
            this.tbEmailU.TabIndex = 8;
            // 
            // lbEmailU
            // 
            this.lbEmailU.AutoSize = true;
            this.lbEmailU.Location = new System.Drawing.Point(266, 71);
            this.lbEmailU.Name = "lbEmailU";
            this.lbEmailU.Size = new System.Drawing.Size(42, 16);
            this.lbEmailU.TabIndex = 12;
            this.lbEmailU.Text = "Email";
            // 
            // tbPhoneU
            // 
            this.tbPhoneU.Enabled = false;
            this.tbPhoneU.Location = new System.Drawing.Point(358, 105);
            this.tbPhoneU.Name = "tbPhoneU";
            this.tbPhoneU.Size = new System.Drawing.Size(141, 22);
            this.tbPhoneU.TabIndex = 9;
            // 
            // lbPhoneU
            // 
            this.lbPhoneU.AutoSize = true;
            this.lbPhoneU.Location = new System.Drawing.Point(266, 108);
            this.lbPhoneU.Name = "lbPhoneU";
            this.lbPhoneU.Size = new System.Drawing.Size(86, 16);
            this.lbPhoneU.TabIndex = 14;
            this.lbPhoneU.Text = "Số điện thoại";
            // 
            // tbNameU
            // 
            this.tbNameU.Enabled = false;
            this.tbNameU.Location = new System.Drawing.Point(102, 105);
            this.tbNameU.Name = "tbNameU";
            this.tbNameU.Size = new System.Drawing.Size(141, 22);
            this.tbNameU.TabIndex = 6;
            // 
            // lbNameU
            // 
            this.lbNameU.AutoSize = true;
            this.lbNameU.Location = new System.Drawing.Point(7, 108);
            this.lbNameU.Name = "lbNameU";
            this.lbNameU.Size = new System.Drawing.Size(32, 16);
            this.lbNameU.TabIndex = 8;
            this.lbNameU.Text = "Tên";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewM);
            this.tabPage1.Controls.Add(this.gbFuntionM);
            this.tabPage1.Controls.Add(this.gbSearchM);
            this.tabPage1.Controls.Add(this.lbMainTitleM);
            this.tabPage1.Controls.Add(this.gbDetailM);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(909, 672);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Quản lí nhạc";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewM
            // 
            this.dataGridViewM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewM.Location = new System.Drawing.Point(9, 485);
            this.dataGridViewM.Name = "dataGridViewM";
            this.dataGridViewM.Size = new System.Drawing.Size(885, 180);
            this.dataGridViewM.TabIndex = 4;
            // 
            // gbFuntionM
            // 
            this.gbFuntionM.Controls.Add(this.btnLogOutM);
            this.gbFuntionM.Controls.Add(this.btListenM);
            this.gbFuntionM.Controls.Add(this.btSaveM);
            this.gbFuntionM.Controls.Add(this.btDelM);
            this.gbFuntionM.Controls.Add(this.btUpdateM);
            this.gbFuntionM.Controls.Add(this.btAddM);
            this.gbFuntionM.Location = new System.Drawing.Point(759, 40);
            this.gbFuntionM.Name = "gbFuntionM";
            this.gbFuntionM.Size = new System.Drawing.Size(135, 332);
            this.gbFuntionM.TabIndex = 5;
            this.gbFuntionM.TabStop = false;
            this.gbFuntionM.Text = "Tính năng";
            // 
            // btnLogOutM
            // 
            this.btnLogOutM.Location = new System.Drawing.Point(30, 222);
            this.btnLogOutM.Name = "btnLogOutM";
            this.btnLogOutM.Size = new System.Drawing.Size(75, 23);
            this.btnLogOutM.TabIndex = 5;
            this.btnLogOutM.Text = "Đăng xuất";
            this.btnLogOutM.UseVisualStyleBackColor = true;
            // 
            // btListenM
            // 
            this.btListenM.Location = new System.Drawing.Point(30, 182);
            this.btListenM.Name = "btListenM";
            this.btListenM.Size = new System.Drawing.Size(75, 23);
            this.btListenM.TabIndex = 4;
            this.btListenM.Text = "Nghe";
            this.btListenM.UseVisualStyleBackColor = true;
            // 
            // btSaveM
            // 
            this.btSaveM.Location = new System.Drawing.Point(30, 143);
            this.btSaveM.Name = "btSaveM";
            this.btSaveM.Size = new System.Drawing.Size(75, 23);
            this.btSaveM.TabIndex = 3;
            this.btSaveM.Text = "Lưu";
            this.btSaveM.UseVisualStyleBackColor = true;
            // 
            // btDelM
            // 
            this.btDelM.Location = new System.Drawing.Point(30, 106);
            this.btDelM.Name = "btDelM";
            this.btDelM.Size = new System.Drawing.Size(75, 23);
            this.btDelM.TabIndex = 2;
            this.btDelM.Text = "Xóa";
            this.btDelM.UseVisualStyleBackColor = true;
            // 
            // btUpdateM
            // 
            this.btUpdateM.Location = new System.Drawing.Point(30, 70);
            this.btUpdateM.Name = "btUpdateM";
            this.btUpdateM.Size = new System.Drawing.Size(75, 23);
            this.btUpdateM.TabIndex = 1;
            this.btUpdateM.Text = "Sửa";
            this.btUpdateM.UseVisualStyleBackColor = true;
            // 
            // btAddM
            // 
            this.btAddM.Location = new System.Drawing.Point(30, 32);
            this.btAddM.Name = "btAddM";
            this.btAddM.Size = new System.Drawing.Size(75, 23);
            this.btAddM.TabIndex = 0;
            this.btAddM.Text = "Thêm";
            this.btAddM.UseVisualStyleBackColor = true;
            // 
            // gbSearchM
            // 
            this.gbSearchM.Controls.Add(this.lbToSearchM);
            this.gbSearchM.Controls.Add(this.lbFromSearchM);
            this.gbSearchM.Controls.Add(this.dtToSearchM);
            this.gbSearchM.Controls.Add(this.dtFromSearchM);
            this.gbSearchM.Controls.Add(this.tbSearchM);
            this.gbSearchM.Controls.Add(this.rbDateSearchM);
            this.gbSearchM.Controls.Add(this.label13);
            this.gbSearchM.Controls.Add(this.rbNameSearchM);
            this.gbSearchM.Controls.Add(this.rbIDM);
            this.gbSearchM.Controls.Add(this.lbNameSearchM);
            this.gbSearchM.Controls.Add(this.lbIDSearchM);
            this.gbSearchM.Location = new System.Drawing.Point(8, 378);
            this.gbSearchM.Name = "gbSearchM";
            this.gbSearchM.Size = new System.Drawing.Size(886, 100);
            this.gbSearchM.TabIndex = 3;
            this.gbSearchM.TabStop = false;
            this.gbSearchM.Text = "Tìm kiếm";
            // 
            // lbToSearchM
            // 
            this.lbToSearchM.AutoSize = true;
            this.lbToSearchM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbToSearchM.Location = new System.Drawing.Point(625, 55);
            this.lbToSearchM.Name = "lbToSearchM";
            this.lbToSearchM.Size = new System.Drawing.Size(30, 15);
            this.lbToSearchM.TabIndex = 9;
            this.lbToSearchM.Text = "Đến";
            // 
            // lbFromSearchM
            // 
            this.lbFromSearchM.AutoSize = true;
            this.lbFromSearchM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFromSearchM.Location = new System.Drawing.Point(446, 55);
            this.lbFromSearchM.Name = "lbFromSearchM";
            this.lbFromSearchM.Size = new System.Drawing.Size(21, 15);
            this.lbFromSearchM.TabIndex = 7;
            this.lbFromSearchM.Text = "Từ";
            // 
            // dtToSearchM
            // 
            this.dtToSearchM.Location = new System.Drawing.Point(661, 55);
            this.dtToSearchM.Name = "dtToSearchM";
            this.dtToSearchM.Size = new System.Drawing.Size(145, 20);
            this.dtToSearchM.TabIndex = 10;
            // 
            // dtFromSearchM
            // 
            this.dtFromSearchM.Location = new System.Drawing.Point(473, 55);
            this.dtFromSearchM.Name = "dtFromSearchM";
            this.dtFromSearchM.Size = new System.Drawing.Size(145, 20);
            this.dtFromSearchM.TabIndex = 8;
            // 
            // tbSearchM
            // 
            this.tbSearchM.Location = new System.Drawing.Point(18, 55);
            this.tbSearchM.Name = "tbSearchM";
            this.tbSearchM.Size = new System.Drawing.Size(248, 20);
            this.tbSearchM.TabIndex = 4;
            // 
            // rbDateSearchM
            // 
            this.rbDateSearchM.AutoSize = true;
            this.rbDateSearchM.Location = new System.Drawing.Point(451, 27);
            this.rbDateSearchM.Name = "rbDateSearchM";
            this.rbDateSearchM.Size = new System.Drawing.Size(14, 13);
            this.rbDateSearchM.TabIndex = 5;
            this.rbDateSearchM.TabStop = true;
            this.rbDateSearchM.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(471, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(131, 15);
            this.label13.TabIndex = 6;
            this.label13.Text = "tìm kiếm theo thời gian";
            // 
            // rbNameSearchM
            // 
            this.rbNameSearchM.AutoSize = true;
            this.rbNameSearchM.Location = new System.Drawing.Point(159, 27);
            this.rbNameSearchM.Name = "rbNameSearchM";
            this.rbNameSearchM.Size = new System.Drawing.Size(14, 13);
            this.rbNameSearchM.TabIndex = 2;
            this.rbNameSearchM.TabStop = true;
            this.rbNameSearchM.UseVisualStyleBackColor = true;
            // 
            // rbIDM
            // 
            this.rbIDM.AutoSize = true;
            this.rbIDM.Location = new System.Drawing.Point(18, 23);
            this.rbIDM.Name = "rbIDM";
            this.rbIDM.Size = new System.Drawing.Size(14, 13);
            this.rbIDM.TabIndex = 0;
            this.rbIDM.TabStop = true;
            this.rbIDM.UseVisualStyleBackColor = true;
            // 
            // lbNameSearchM
            // 
            this.lbNameSearchM.AutoSize = true;
            this.lbNameSearchM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNameSearchM.Location = new System.Drawing.Point(179, 25);
            this.lbNameSearchM.Name = "lbNameSearchM";
            this.lbNameSearchM.Size = new System.Drawing.Size(68, 15);
            this.lbNameSearchM.TabIndex = 3;
            this.lbNameSearchM.Text = "Tên bài hát";
            // 
            // lbIDSearchM
            // 
            this.lbIDSearchM.AutoSize = true;
            this.lbIDSearchM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIDSearchM.Location = new System.Drawing.Point(38, 21);
            this.lbIDSearchM.Name = "lbIDSearchM";
            this.lbIDSearchM.Size = new System.Drawing.Size(25, 15);
            this.lbIDSearchM.TabIndex = 1;
            this.lbIDSearchM.Text = "Mã";
            // 
            // lbMainTitleM
            // 
            this.lbMainTitleM.AutoSize = true;
            this.lbMainTitleM.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitleM.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbMainTitleM.Location = new System.Drawing.Point(175, 18);
            this.lbMainTitleM.Name = "lbMainTitleM";
            this.lbMainTitleM.Size = new System.Drawing.Size(329, 18);
            this.lbMainTitleM.TabIndex = 1;
            this.lbMainTitleM.Text = "Quản lí danh sách bài hát trong kho dữ liệu";
            // 
            // gbDetailM
            // 
            this.gbDetailM.Controls.Add(this.numRateM);
            this.gbDetailM.Controls.Add(this.dtDateCreateM);
            this.gbDetailM.Controls.Add(this.lbDateM);
            this.gbDetailM.Controls.Add(this.tbTypeM);
            this.gbDetailM.Controls.Add(this.lbTypeM);
            this.gbDetailM.Controls.Add(this.tbLyricM);
            this.gbDetailM.Controls.Add(this.lbLyricM);
            this.gbDetailM.Controls.Add(this.lbRateM);
            this.gbDetailM.Controls.Add(this.tbAlbumM);
            this.gbDetailM.Controls.Add(this.lbAlbumM);
            this.gbDetailM.Controls.Add(this.tbSingerM);
            this.gbDetailM.Controls.Add(this.lbSingerM);
            this.gbDetailM.Controls.Add(this.tbAuthorM);
            this.gbDetailM.Controls.Add(this.lbAuthorM);
            this.gbDetailM.Controls.Add(this.tbNameM);
            this.gbDetailM.Controls.Add(this.lbNameM);
            this.gbDetailM.Controls.Add(this.tbIDM);
            this.gbDetailM.Controls.Add(this.lbIDM);
            this.gbDetailM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDetailM.ForeColor = System.Drawing.SystemColors.Desktop;
            this.gbDetailM.Location = new System.Drawing.Point(8, 39);
            this.gbDetailM.Name = "gbDetailM";
            this.gbDetailM.Size = new System.Drawing.Size(735, 333);
            this.gbDetailM.TabIndex = 2;
            this.gbDetailM.TabStop = false;
            this.gbDetailM.Text = "Thông tin chi tiết";
            // 
            // numRateM
            // 
            this.numRateM.Location = new System.Drawing.Point(316, 107);
            this.numRateM.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numRateM.Name = "numRateM";
            this.numRateM.Size = new System.Drawing.Size(145, 22);
            this.numRateM.TabIndex = 11;
            // 
            // dtDateCreateM
            // 
            this.dtDateCreateM.Location = new System.Drawing.Point(558, 60);
            this.dtDateCreateM.Name = "dtDateCreateM";
            this.dtDateCreateM.Size = new System.Drawing.Size(145, 22);
            this.dtDateCreateM.TabIndex = 15;
            // 
            // lbDateM
            // 
            this.lbDateM.AutoSize = true;
            this.lbDateM.Location = new System.Drawing.Point(493, 62);
            this.lbDateM.Name = "lbDateM";
            this.lbDateM.Size = new System.Drawing.Size(63, 16);
            this.lbDateM.TabIndex = 14;
            this.lbDateM.Text = "Ngày tạo";
            // 
            // tbTypeM
            // 
            this.tbTypeM.Location = new System.Drawing.Point(557, 19);
            this.tbTypeM.Name = "tbTypeM";
            this.tbTypeM.Size = new System.Drawing.Size(145, 22);
            this.tbTypeM.TabIndex = 13;
            // 
            // lbTypeM
            // 
            this.lbTypeM.AutoSize = true;
            this.lbTypeM.Location = new System.Drawing.Point(493, 22);
            this.lbTypeM.Name = "lbTypeM";
            this.lbTypeM.Size = new System.Drawing.Size(57, 16);
            this.lbTypeM.TabIndex = 12;
            this.lbTypeM.Text = "Thể loại";
            // 
            // tbLyricM
            // 
            this.tbLyricM.Location = new System.Drawing.Point(71, 151);
            this.tbLyricM.Multiline = true;
            this.tbLyricM.Name = "tbLyricM";
            this.tbLyricM.Size = new System.Drawing.Size(632, 165);
            this.tbLyricM.TabIndex = 17;
            // 
            // lbLyricM
            // 
            this.lbLyricM.AutoSize = true;
            this.lbLyricM.Location = new System.Drawing.Point(7, 151);
            this.lbLyricM.Name = "lbLyricM";
            this.lbLyricM.Size = new System.Drawing.Size(58, 16);
            this.lbLyricM.TabIndex = 16;
            this.lbLyricM.Text = "Lời nhạc";
            // 
            // lbRateM
            // 
            this.lbRateM.AutoSize = true;
            this.lbRateM.Location = new System.Drawing.Point(255, 109);
            this.lbRateM.Name = "lbRateM";
            this.lbRateM.Size = new System.Drawing.Size(61, 16);
            this.lbRateM.TabIndex = 10;
            this.lbRateM.Text = "Đánh giá";
            // 
            // tbAlbumM
            // 
            this.tbAlbumM.Location = new System.Drawing.Point(316, 59);
            this.tbAlbumM.Name = "tbAlbumM";
            this.tbAlbumM.Size = new System.Drawing.Size(145, 22);
            this.tbAlbumM.TabIndex = 9;
            // 
            // lbAlbumM
            // 
            this.lbAlbumM.AutoSize = true;
            this.lbAlbumM.Location = new System.Drawing.Point(255, 62);
            this.lbAlbumM.Name = "lbAlbumM";
            this.lbAlbumM.Size = new System.Drawing.Size(46, 16);
            this.lbAlbumM.TabIndex = 8;
            this.lbAlbumM.Text = "Album";
            // 
            // tbSingerM
            // 
            this.tbSingerM.Location = new System.Drawing.Point(316, 19);
            this.tbSingerM.Name = "tbSingerM";
            this.tbSingerM.Size = new System.Drawing.Size(145, 22);
            this.tbSingerM.TabIndex = 7;
            // 
            // lbSingerM
            // 
            this.lbSingerM.AutoSize = true;
            this.lbSingerM.Location = new System.Drawing.Point(255, 22);
            this.lbSingerM.Name = "lbSingerM";
            this.lbSingerM.Size = new System.Drawing.Size(40, 16);
            this.lbSingerM.TabIndex = 6;
            this.lbSingerM.Text = "Ca sĩ";
            // 
            // tbAuthorM
            // 
            this.tbAuthorM.Location = new System.Drawing.Point(68, 103);
            this.tbAuthorM.Name = "tbAuthorM";
            this.tbAuthorM.Size = new System.Drawing.Size(156, 22);
            this.tbAuthorM.TabIndex = 5;
            // 
            // lbAuthorM
            // 
            this.lbAuthorM.AutoSize = true;
            this.lbAuthorM.Location = new System.Drawing.Point(7, 106);
            this.lbAuthorM.Name = "lbAuthorM";
            this.lbAuthorM.Size = new System.Drawing.Size(55, 16);
            this.lbAuthorM.TabIndex = 4;
            this.lbAuthorM.Text = "Nhạc sĩ";
            // 
            // tbNameM
            // 
            this.tbNameM.Location = new System.Drawing.Point(68, 59);
            this.tbNameM.Name = "tbNameM";
            this.tbNameM.Size = new System.Drawing.Size(156, 22);
            this.tbNameM.TabIndex = 3;
            // 
            // lbNameM
            // 
            this.lbNameM.AutoSize = true;
            this.lbNameM.Location = new System.Drawing.Point(7, 62);
            this.lbNameM.Name = "lbNameM";
            this.lbNameM.Size = new System.Drawing.Size(32, 16);
            this.lbNameM.TabIndex = 2;
            this.lbNameM.Text = "Tên";
            // 
            // tbIDM
            // 
            this.tbIDM.Location = new System.Drawing.Point(68, 19);
            this.tbIDM.Name = "tbIDM";
            this.tbIDM.Size = new System.Drawing.Size(156, 22);
            this.tbIDM.TabIndex = 1;
            // 
            // lbIDM
            // 
            this.lbIDM.AutoSize = true;
            this.lbIDM.Location = new System.Drawing.Point(7, 22);
            this.lbIDM.Name = "lbIDM";
            this.lbIDM.Size = new System.Drawing.Size(27, 16);
            this.lbIDM.TabIndex = 0;
            this.lbIDM.Text = "Mã";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Location = new System.Drawing.Point(0, 1);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(917, 698);
            this.tabControl.TabIndex = 0;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
            // 
            // frmManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(918, 702);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lí hoạt động nghe nhạc";
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListUserU)).EndInit();
            this.gbConfirmU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfirmU)).EndInit();
            this.gbFuntionU.ResumeLayout(false);
            this.gbSearchU.ResumeLayout(false);
            this.gbSearchU.PerformLayout();
            this.gbDetailU.ResumeLayout(false);
            this.gbDetailU.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewM)).EndInit();
            this.gbFuntionM.ResumeLayout(false);
            this.gbSearchM.ResumeLayout(false);
            this.gbSearchM.PerformLayout();
            this.gbDetailM.ResumeLayout(false);
            this.gbDetailM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRateM)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvListUserU;
        private System.Windows.Forms.GroupBox gbConfirmU;
        private System.Windows.Forms.DataGridView dgvConfirmU;
        private System.Windows.Forms.GroupBox gbFuntionU;
        private System.Windows.Forms.Button btnLogOutU;
        private System.Windows.Forms.Button btnSaveU;
        private System.Windows.Forms.Button btnDelU;
        private System.Windows.Forms.Button btnUpdateU;
        private System.Windows.Forms.Button btnAddU;
        private System.Windows.Forms.GroupBox gbSearchU;
        private System.Windows.Forms.Label lbToSearchU;
        private System.Windows.Forms.Label lbFromSearchU;
        private System.Windows.Forms.DateTimePicker dtToSearchU;
        private System.Windows.Forms.DateTimePicker dtFromSearchU;
        private System.Windows.Forms.TextBox tbSearchU;
        private System.Windows.Forms.RadioButton rbDateSearchU;
        private System.Windows.Forms.Label lbDateSearchU;
        private System.Windows.Forms.RadioButton rbNameSearchU;
        private System.Windows.Forms.RadioButton rbUserNameSearchU;
        private System.Windows.Forms.Label lbNameSearchU;
        private System.Windows.Forms.Label lbUserNameSearchU;
        private System.Windows.Forms.Label lbMainTitleU;
        private System.Windows.Forms.GroupBox gbDetailU;
        private System.Windows.Forms.TextBox tbPwdU;
        private System.Windows.Forms.Label lbPassU;
        private System.Windows.Forms.TextBox tbUserNameU;
        private System.Windows.Forms.Label lbUserNameU;
        private System.Windows.Forms.DateTimePicker dtDateCreateU;
        private System.Windows.Forms.Label lbDateCreateU;
        private System.Windows.Forms.TextBox tbNoteU;
        private System.Windows.Forms.Label lbNoteU;
        private System.Windows.Forms.TextBox tbAgeU;
        private System.Windows.Forms.Label lbAgeU;
        private System.Windows.Forms.TextBox tbAdrrU;
        private System.Windows.Forms.Label lbAdrrU;
        private System.Windows.Forms.TextBox tbEmailU;
        private System.Windows.Forms.Label lbEmailU;
        private System.Windows.Forms.TextBox tbPhoneU;
        private System.Windows.Forms.Label lbPhoneU;
        private System.Windows.Forms.TextBox tbNameU;
        private System.Windows.Forms.Label lbNameU;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridViewM;
        private System.Windows.Forms.GroupBox gbFuntionM;
        private System.Windows.Forms.Button btnLogOutM;
        private System.Windows.Forms.Button btListenM;
        private System.Windows.Forms.Button btSaveM;
        private System.Windows.Forms.Button btDelM;
        private System.Windows.Forms.Button btUpdateM;
        private System.Windows.Forms.Button btAddM;
        private System.Windows.Forms.GroupBox gbSearchM;
        private System.Windows.Forms.Label lbToSearchM;
        private System.Windows.Forms.Label lbFromSearchM;
        private System.Windows.Forms.DateTimePicker dtToSearchM;
        private System.Windows.Forms.DateTimePicker dtFromSearchM;
        private System.Windows.Forms.TextBox tbSearchM;
        private System.Windows.Forms.RadioButton rbDateSearchM;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton rbNameSearchM;
        private System.Windows.Forms.RadioButton rbIDM;
        private System.Windows.Forms.Label lbNameSearchM;
        private System.Windows.Forms.Label lbIDSearchM;
        private System.Windows.Forms.Label lbMainTitleM;
        private System.Windows.Forms.GroupBox gbDetailM;
        private System.Windows.Forms.NumericUpDown numRateM;
        private System.Windows.Forms.DateTimePicker dtDateCreateM;
        private System.Windows.Forms.Label lbDateM;
        private System.Windows.Forms.TextBox tbTypeM;
        private System.Windows.Forms.Label lbTypeM;
        private System.Windows.Forms.TextBox tbLyricM;
        private System.Windows.Forms.Label lbLyricM;
        private System.Windows.Forms.Label lbRateM;
        private System.Windows.Forms.TextBox tbAlbumM;
        private System.Windows.Forms.Label lbAlbumM;
        private System.Windows.Forms.TextBox tbSingerM;
        private System.Windows.Forms.Label lbSingerM;
        private System.Windows.Forms.TextBox tbAuthorM;
        private System.Windows.Forms.Label lbAuthorM;
        private System.Windows.Forms.TextBox tbNameM;
        private System.Windows.Forms.Label lbNameM;
        private System.Windows.Forms.TextBox tbIDM;
        private System.Windows.Forms.Label lbIDM;
        private System.Windows.Forms.TabControl tabControl;


    }
}

