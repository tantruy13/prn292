﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PROJECTLAST.DAL;
using PROJECTLAST.ENTITIES;

namespace PROJECTLAST
{
    public partial class frmManage : Form
    {
        bool isSearch = false;
        string orderBy = "";
        public frmManage()
        {
            InitializeComponent();
            
        }

        
        
        // nếu tabPage được click
        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            
            if (e.TabPageIndex == 0)
            {

            }
            if (e.TabPageIndex == 1)
            {
                //comboBox1.DataSource = DAO.GetAllCategories();
                //comboBox1.DisplayMember = "Name";
                //comboBox1.SelectedIndex = 0;


                //dgvListUserU.DataSource = DAO.GetAllUser();


                // load data vào datagridview UserList bên tabpage 2
                dgvConfirmU.AutoGenerateColumns = false;
                dgvConfirmU.DataSource = UserDAL.GetAllUser("0");
                loadGridViewData(dgvConfirmU);
                // load data vào datagridview UserConfrim bên tabpage 2
                dgvListUserU.AutoGenerateColumns = false;
                dgvListUserU.DataSource = UserDAL.GetAllUser("1");
                loadGridViewData(dgvListUserU);

            }
        }
        private void loadGridViewData(DataGridView dgv)
        {

            

            dgv.Columns.Add("UserID", "UserID ↑");
            dgv.Columns["UserID"].DataPropertyName = "userID";

            dgv.Columns.Add("UserName", "UserName ↑");
            dgv.Columns["UserName"].DataPropertyName = "userName";

            dgv.Columns.Add("UserPassword", "UserPassword");
            dgv.Columns["UserPassword"].DataPropertyName = "userPw";

            dgv.Columns.Add("UserAge", "UserAge ↑");
            dgv.Columns["UserAge"].DataPropertyName = "userAge";

            dgv.Columns.Add("userEmail", "userEmail");
            dgv.Columns["userEmail"].DataPropertyName = "userEmail";

            dgv.Columns.Add("UserPhone", "UserPhone");
            dgv.Columns["UserPhone"].DataPropertyName = "userPhone";

            dgv.Columns.Add("UserAdress", "UserAdress");
            dgv.Columns["UserAdress"].DataPropertyName = "userAdress";

            dgv.Columns.Add("DateCreate", "DateCreate");
            dgv.Columns["DateCreate"].DataPropertyName = "dateCreate";
            dgv.Columns["DateCreate"].DefaultCellStyle.Format = "dd/MM/yyyy";

            dgv.Columns.Add("Note", "Note");
            dgv.Columns["Note"].DataPropertyName = "note";

        }

        private void dgvListUserU_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                //int catID = ((Category)comboBox1.SelectedItem).Id;
                if (e.ColumnIndex == 0)
                {
                    if (isSearch)
                    {
                        if (dgvListUserU.Columns[0].HeaderText.Equals("UserID ↓"))
                        {
                            dgvListUserU.Columns[0].HeaderText = "UserID ↑";
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserID", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductID";
                        }
                        else
                        {
                            dgvListUserU.Columns[0].HeaderText = ("UserID ↓");
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserID", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductID";
                        }
                    }
                    else
                    {
                        if (dgvListUserU.Columns[0].HeaderText.Equals("UserID ↓"))
                        {
                            dgvListUserU.Columns[0].HeaderText = "UserID ↑";
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserID", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductID";
                        }
                        else
                        {
                            dgvListUserU.Columns[0].HeaderText = ("UserID ↓");
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserID", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductID";
                        }
                    }
                }
                else if (e.ColumnIndex == 1)
                {
                    if (isSearch)
                    {
                        if (dgvListUserU.Columns[1].HeaderText.Equals("UserName ↓"))
                        {
                            dgvListUserU.Columns[1].HeaderText = "UserName ↑";
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","Name", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductName";
                        }
                        else
                        {
                            dgvListUserU.Columns[1].HeaderText = ("UserName ↓");
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","Name", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductName";
                        }
                    }
                    else
                    {
                        if (dgvListUserU.Columns[1].HeaderText.Equals("UserName ↓"))
                        {
                            dgvListUserU.Columns[1].HeaderText = "UserName ↑";
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","Name", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductName";

                        }
                        else
                        {
                            dgvListUserU.Columns[1].HeaderText = ("UserName ↓");
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","Name", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductName";
                        }
                    }
                }
                else if (e.ColumnIndex == 3)
                {
                    if (isSearch)
                    {
                        if (dgvListUserU.Columns[3].HeaderText.Equals("UserAge ↓"))
                        {
                            dgvListUserU.Columns[3].HeaderText = "UserAge ↑";
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserAge", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "SuppplierID";
                        }
                        else
                        {
                            dgvListUserU.Columns[3].HeaderText = ("UserAge ↓");
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserAge", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "SuppplierID";
                        }
                    }
                    else
                    {
                        if (dgvListUserU.Columns[3].HeaderText.Equals("UserAge ↓"))
                        {
                            dgvListUserU.Columns[3].HeaderText = "UserAge ↑";
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserAge", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "SuppplierID";
                        }
                        else
                        {
                            dgvListUserU.Columns[3].HeaderText = ("UserAge ↓");
                            dgvListUserU.DataSource = UserDAL.GetAllUserOrderby("1","UserAge", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "SuppplierID";
                        }
                    }
                }
                
            }
            else
            {
                //tbUserNameU.Enabled = true;
                //tbUserNameU.Text = e.ColumnIndex+"";
                //Console.WriteLine(e.ColumnIndex + "row index = " + e.RowIndex);

                //foreach (DataGridViewRow row in dgvListUserU.Rows)
                //{
                //    if (row.Selected)
                //    {
                        
                //        //foreach (DataGridViewCell cell in row.Cells)
                //        //{
                            
                //        //    int index = cell.ColumnIndex;
                //        //    if (index == 1)
                //        //    {
                                
                //        //        //do what you want with the value
                //        //    }
                //        //}
                //    }
                //}
               
            }
        }

        private void dgvConfirmU_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                //int catID = ((Category)comboBox1.SelectedItem).Id;
                if (e.ColumnIndex == 0)
                {
                    if (isSearch)
                    {
                        if (dgvConfirmU.Columns[0].HeaderText.Equals("UserID ↓"))
                        {
                            dgvConfirmU.Columns[0].HeaderText = "UserID ↑";
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserID", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductID";

                        }
                        else
                        {
                            dgvConfirmU.Columns[0].HeaderText = ("UserID ↓");
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserID", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductID";
                        }
                    }
                    else
                    {
                        if (dgvConfirmU.Columns[0].HeaderText.Equals("UserID ↓"))
                        {
                            dgvConfirmU.Columns[0].HeaderText = "UserID ↑";
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserID", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductID";
                        }
                        else
                        {
                            dgvConfirmU.Columns[0].HeaderText = ("UserID ↓");
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserID", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductID";
                        }
                    }
                }
                else if (e.ColumnIndex == 1)
                {
                    if (isSearch)
                    {
                        if (dgvConfirmU.Columns[1].HeaderText.Equals("UserName ↓"))
                        {
                            dgvConfirmU.Columns[1].HeaderText = "UserName ↑";
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "Name", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductName";
                        }
                        else
                        {
                            dgvConfirmU.Columns[1].HeaderText = ("UserName ↓");
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "Name", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductName";
                        }
                    }
                    else
                    {
                        if (dgvConfirmU.Columns[1].HeaderText.Equals("UserName ↓"))
                        {
                            dgvConfirmU.Columns[1].HeaderText = "UserName ↑";
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "Name", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "ProductName";

                        }
                        else
                        {
                            dgvConfirmU.Columns[1].HeaderText = ("UserName ↓");
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "Name", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "ProductName";
                        }
                    }
                }
                else if (e.ColumnIndex == 3)
                {
                    if (isSearch)
                    {
                        if (dgvConfirmU.Columns[3].HeaderText.Equals("UserAge ↓"))
                        {
                            dgvConfirmU.Columns[3].HeaderText = "UserAge ↑";
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserAge", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "SuppplierID";
                        }
                        else
                        {
                            dgvConfirmU.Columns[3].HeaderText = ("UserAge ↓");
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserAge", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "SuppplierID";
                        }
                    }
                    else
                    {
                        if (dgvConfirmU.Columns[3].HeaderText.Equals("UserAge ↓"))
                        {
                            dgvConfirmU.Columns[3].HeaderText = "UserAge ↑";
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserAge", "ASC");
                            orderBy = "ASC";
                            //currentColumn = "SuppplierID";
                        }
                        else
                        {
                            dgvConfirmU.Columns[3].HeaderText = ("UserAge ↓");
                            dgvConfirmU.DataSource = UserDAL.GetAllUserOrderby("0", "UserAge", "DESC");
                            orderBy = "DESC";
                            //currentColumn = "SuppplierID";
                        }
                    }
                }

            }
            else
            {

            }
        }

        private void btnUpdateU_Click(object sender, EventArgs e)
        {
            // get dữ liệu từ bảng gridview vào thông tin chi tiết để sửa
            int row = dgvListUserU.CurrentRow.Index;
            tbUserNameU.Text = setText(0,row);
            tbPwdU.Text = setText(1, row);
            tbNameU.Text = setText(2, row);
            tbAgeU.Text = setText(3, row);
            tbEmailU.Text = setText(4, row);
            tbPhoneU.Text = setText(5, row);
            tbAdrrU.Text = setText(6, row);
            dtDateCreateU.Text = setText(7, row);
            tbNoteU.Text = setText(8, row);
            // enable tất cả các input trong thông tin chi tiết để sửa
            enableTrueDetail();
            btnSaveU.Enabled = true;
            
        }
        public void enableFalseDetail()
        {
            tbUserNameU.Enabled = false;
            tbPwdU.Enabled = false;
            tbNameU.Enabled = false;
            tbAgeU.Enabled = false;
            tbEmailU.Enabled = false;
            tbPhoneU.Enabled = false;
            tbAdrrU.Enabled = false;
            dtDateCreateU.Enabled = false;
            tbNoteU.Enabled = false;
        }
        public void enableTrueDetail()
        {
            tbUserNameU.Enabled = true;
            tbPwdU.Enabled = true;
            tbNameU.Enabled = true;
            tbAgeU.Enabled = true;
            tbEmailU.Enabled = true;
            tbPhoneU.Enabled = true;
            tbAdrrU.Enabled = true;
            dtDateCreateU.Enabled = true;
            tbNoteU.Enabled = true;
        }
        public string setText(int col, int row) 
        {
            return dgvListUserU[col, row].Value.ToString();
        }

        private void btnSaveU_Click(object sender, EventArgs e)
        {

        }
       
        

       

       
    }
}
