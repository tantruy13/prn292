﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PROJECTLAST.ENTITIES
{
    class Users
    {
        public string userID { get; set; }
        public string userPw { get; set; }
        public string userName { get; set; }
        
        public int userAge { get; set; }
        public string userEmail { get; set; }
        public string userPhone { get; set; }
        public string userAdress { get; set; }
        public string dateCreate { get; set; }
        public int status { get; set; }
        public string note { get; set; }

        public Users()
        {
        }
        public Users(string UserId, string UserPw, string UserName, int UserAge, string UserEmail,
            string UserPhone,string UserAdress,string DateCreate,int Status, string Note)
        {
            userID = UserId;
            userName = UserName;
            userPw = UserPw;
            userAge = UserAge;
            userEmail = UserEmail;
            userPhone = UserPhone;
            userAdress = UserAdress;
            dateCreate = DateCreate;
            status = Status;
            note = Note;
        }  
    }
}
