﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace PROJECTLAST.DAL
{
    class DAO
    {
        static public string getConnectionString()
        {
            // return @"server=localhost;database=PRN292;uid=sa;pwd=123456";
            return System.Configuration.ConfigurationManager.ConnectionStrings["test"].ToString();
        }
        static public DataTable getDataTable(string sql)
        {
            SqlConnection con = new SqlConnection(getConnectionString());
            SqlCommand command = new SqlCommand(sql, con);
            SqlDataAdapter adapt = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            return ds.Tables[0];
        }
    }
}
