﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PROJECTLAST.ENTITIES;

namespace PROJECTLAST.DAL
{
    class UserDAL
    {
        static public string getConnectionString()
        {
            // return @"server=localhost;database=PRN292;uid=sa;pwd=123456";
            return System.Configuration.ConfigurationManager.ConnectionStrings["test"].ToString();
        }
        static public DataTable getDataTable(string sql)
        {
            SqlConnection con = new SqlConnection(getConnectionString());
            SqlCommand command = new SqlCommand(sql, con);
            SqlDataAdapter adapt = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            return ds.Tables[0];
        }
        static public List<Users> GetAllUser(string stat)
        {
            string sql = "select * from Users inner join UserLogins on Users.UserID = UserLogins.UserID where Users.Status = " + stat + "";
            DataTable dt = UserDAL.getDataTable(sql);
            List<Users> users = new List<Users>();
            foreach (DataRow dr in dt.Rows)
            {
                String status = "";
                if (dr["Status"].ToString().Equals("true"))
                {
                    status = "1";
                }
                else
                {
                    status = "0";
                }
                users.Add(new Users(dr["UserID"].ToString(), dr["LoginPwd"].ToString(), dr["Name"].ToString(),
                    Int32.Parse(dr["UserAge"].ToString()), dr["UserEmail"].ToString(), dr["UserPhone"].ToString(),
                    dr["UserAdress"].ToString(), dr["CreateDate"].ToString(),
                    Int32.Parse(status), dr["Note"].ToString()));
            }
            return users;
        }
        static public List<Users> GetAllUserOrderby(string stat, string column, string sortUpDown)
        {
            string sql = "select * from Users inner join UserLogins"
            + " on Users.UserID = UserLogins.UserID where Users.Status = " + stat + " order by Users." + column + " " + sortUpDown;
            DataTable dt = UserDAL.getDataTable(sql);
            List<Users> users = new List<Users>();
            foreach (DataRow dr in dt.Rows)
            {
                String status = "";
                if (dr["Status"].ToString().Equals("true"))
                {
                    status = "1";
                }
                else
                {
                    status = "0";
                }
                users.Add(new Users(dr["UserID"].ToString(), dr["LoginPwd"].ToString(), dr["Name"].ToString(),
                    Int32.Parse(dr["UserAge"].ToString()), dr["UserEmail"].ToString(), dr["UserPhone"].ToString(),
                    dr["UserAdress"].ToString(), dr["CreateDate"].ToString(),
                    Int32.Parse(status), dr["Note"].ToString()));
            }
            return users;
        }

        //static public int updateUserInfo(int UserID, string LoginPwd, string Name, int UserAge, string UserEmail, string UserPhone,
        //    string UserAddress, string CreateDate, int Status, string Note)
        //{
        //    String sql = "update Users set UserID=@userid,Name=@name, UserAge=@userage,UserEmail=@useremail,UserPhone=@userphone,"
        //   + "UserAdress=@userAdress,CreateDate=@createdate,Status=@status  where UserID =@useridd";
        //    SqlConnection con = new SqlConnection(getConnectionString());
        //    SqlCommand com = new SqlCommand(sql, con);
        //    SqlParameter param1 = new SqlParameter("@userid", UserID);
        //    param1.SqlDbType = SqlDbType.Text;
        //    SqlParameter param2 = new SqlParameter("@supId", supId);
        //    SqlParameter param3 = new SqlParameter("@catId", catId);
        //    SqlParameter param4 = new SqlParameter("@quan", quan);
        //    SqlParameter param5 = new SqlParameter("@price", uniprice);
        //    SqlParameter param6 = new SqlParameter("@uns", uns);
        //    SqlParameter param7 = new SqlParameter("@uoi", uoi);
        //    SqlParameter param8 = new SqlParameter("@rl", rl);
        //    SqlParameter param9 = new SqlParameter("@dis", dis);
        //    SqlParameter param10 = new SqlParameter("@id", Id);
        //    com.Parameters.Add(param1);
        //    com.Parameters.Add(param2);
        //    com.Parameters.Add(param3);
        //    com.Parameters.Add(param4);
        //    com.Parameters.Add(param5);
        //    com.Parameters.Add(param6);
        //    com.Parameters.Add(param7);
        //    com.Parameters.Add(param8);
        //    com.Parameters.Add(param9);
        //    com.Parameters.Add(param10);

        //    con.Open();
        //    int i = com.ExecuteNonQuery();
        //    con.Close();
        //    return i;


        //}
    }
}
